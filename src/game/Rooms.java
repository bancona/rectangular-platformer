package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import actors.Actor;
import actors.Ground;
import actors.enemies.ArmoredPlatformPacer;
import actors.enemies.PlatformFloater;
import actors.enemies.PlatformPacer;
import actors.hero.GoalOnPlatform;
import actors.hero.Hero;
import actors.platforms.HorizontalMovingPlatform;
import actors.platforms.Platform;
import actors.platforms.VerticalMovingPlatform;

@SuppressWarnings("serial")
public class Rooms {
	public static final Font buttonFont = new Font("Arial Black", Font.BOLD, 25),
			labelFont = new Font("Arial Black", Font.BOLD, 35);
	public static final int BUTTON_HEIGHT = 75, LABEL_HEIGHT = 40,
			BUTTON_TEXT_INSET = 20;
	public static final Insets menuInsets = new Insets(10, 10, 10, 10);
	public static Level currentLevel;

	public static enum Level {
		ONE, TWO, THREE, FOUR;
		public Level getNext() {
			return ordinal() + 1 < values().length ? values()[ordinal() + 1]
					: null;
		}
	}

	public static void loadMainMenu(Screen screen) {
		screen.getContentPane().removeAll();
		screen.clearActors();
		screen.getContentPane().setBackground(Color.BLACK);
		GridBagConstraints c = new GridBagConstraints();
		c.insets = menuInsets;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 0;
		MenuLabel mainLabel = new MenuLabel("MAIN MENU");
		screen.getContentPane().add(mainLabel, c);
		screen.validate();
		c.gridy++;
		MenuButton playButton = new MenuButton("PLAY");
		screen.getContentPane().add(playButton, c);
		screen.validate();
		MenuButton levelSelectButton = new MenuButton("LEVEL SELECT");
		c.gridy++;
		screen.getContentPane().add(levelSelectButton, c);
		screen.validate();
		MenuButton instructionsButton = new MenuButton("INSTRUCTIONS");
		c.gridy++;
		screen.getContentPane().add(instructionsButton, c);
		screen.validate();
		MenuButton optionsButton = new MenuButton("OPTIONS");
		c.gridy++;
		screen.getContentPane().add(optionsButton, c);
		screen.validate();
		while (playButton.isEnabled()) {
			screen.repaint();
		}
		loadRoom(Level.ONE, screen);
	}

	public static void loadRoom(Level lvl, Screen screen) {
		Camera.reset();
		if (lvl == Level.ONE) {
			loadRoomOne(screen);
		} else if (lvl == Level.TWO) {
			loadRoomTwo(screen);
		} else {
			loadWonMenu(screen);
		}
	}

	public static void loadWonMenu(Screen screen) {
		screen.getContentPane().removeAll();
		GridBagConstraints c = new GridBagConstraints();
		c.insets = menuInsets;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 0;
		MenuLabel congratsLabel = new MenuLabel("CONGRATULATIONS!");
		screen.getContentPane().add(congratsLabel, c);
		screen.validate();
		c.gridy++;
		MenuLabel gameWonLabel = new MenuLabel("YOU BEAT THE GAME!");
		screen.getContentPane().add(gameWonLabel, c);
		screen.validate();
		c.gridy++;
		MenuButton quitButton = new MenuButton("MAIN MENU");
		screen.getContentPane().add(quitButton, c);
		screen.validate();
		c.gridy++;
		MenuButton restartButton = new MenuButton("RESTART GAME");
		screen.getContentPane().add(restartButton, c);
		screen.validate();
		screen.requestFocus();
		while (quitButton.isEnabled() && restartButton.isEnabled()) {
			Screen.sleep(1, 0);
		}
		if (quitButton.isEnabled() == false) {
			loadMainMenu(screen);
		} else if (restartButton.isEnabled() == false) {
			loadRoom(Level.ONE, screen);
		}
	}

	public static void loadRoomOne(Screen screen) {
		currentLevel = Level.ONE;
		screen.getContentPane().removeAll();
		screen.clearActors();
		screen.getContentPane().setBackground(Screen.BACKGROUND_COLOR);
		ArrayList<Actor> actors = new ArrayList<Actor>();
		actors.add(new Ground(screen));
		actors.add(new Hero((Screen.MIN_WIDTH - 20) / 2,
				Screen.GROUND_LEVEL - 30, Color.BLUE, screen));
		// for (int i = 3; i < 8; i++) {
		// actors.add(new PolyActor(100 + 50 * (i - 3),
		// Screen.GROUND_LEVEL - 30, 20, i, Color.BLUE, screen));
		// }
		actors.add(new Platform(100, 400, 200, 15, Color.GRAY, screen));
		Platform p = new Platform(300, 369, 200, 15, Color.GRAY, screen);
		actors.add(p);
		actors.add(new ArmoredPlatformPacer(100, 400, Color.RED, screen, p));
		actors.add(new PlatformFloater(100, 400, Color.RED, screen, p));
		p = new HorizontalMovingPlatform(300, 169, 200, 15, Color.GRAY, screen);
		actors.add(p);
		actors.add(new PlatformPacer(100, 400, Color.RED, screen, p));
		p = new VerticalMovingPlatform(100, 115, 200, 15, Color.GRAY, screen);
		actors.add(p);
		actors.add(new PlatformPacer(100, 400, Color.RED, screen, p));
		actors.add(p = new VerticalMovingPlatform(100, 115 - 200, 200, 15,
				Color.GRAY, screen));
		actors.add(new VerticalMovingPlatform(100, 115 - 400, 200, 15,
				Color.GRAY, screen));
		actors.add(new GoalOnPlatform(0, 0, Color.GREEN, screen, p));
		actors.add(new VerticalMovingPlatform(100, 115 - 600, 200, 15,
				Color.GRAY, screen));
		actors.add(new VerticalMovingPlatform(100, 115 - 800, 200, 15,
				Color.GRAY, screen));
		screen.addActors(actors);
		screen.paused = false;
		screen.play();
	}

	public static void loadRoomTwo(Screen screen) {
		currentLevel = Level.TWO;
		screen.getContentPane().removeAll();
		screen.clearActors();
		screen.getContentPane().setBackground(Screen.BACKGROUND_COLOR);
		ArrayList<Actor> actors = new ArrayList<Actor>();
		actors.add(new Ground(screen));
		actors.add(new Hero((Screen.MIN_WIDTH - 20) / 2,
				Screen.GROUND_LEVEL - 30, Color.BLUE, screen));
		actors.add(new Platform(100, 400, 200, 15, Color.BLACK, screen));
		Platform p = new Platform(300, 369, 200, 15, Color.BLACK, screen);
		actors.add(p);
		// actors.add(new PlatformPacer(100, 400, Color.RED, screen, p));
		// actors.add(new PlatformFloater(100, 400, Color.RED, screen, p));
		p = new HorizontalMovingPlatform(300, 169, 200, 15, Color.BLACK, screen);
		actors.add(p);
		// actors.add(new PlatformPacer(100, 400, Color.RED, screen, p));
		p = new VerticalMovingPlatform(100, 115, 200, 15, Color.BLACK, screen);
		actors.add(p);
		// actors.add(new PlatformPacer(100, 400, Color.RED, screen, p));
		actors.add(p = new VerticalMovingPlatform(100, 115 - 200, 200, 15,
				Color.BLACK, screen));
		actors.add(new VerticalMovingPlatform(100, 115 - 400, 200, 15,
				Color.BLACK, screen));
		actors.add(new GoalOnPlatform(0, 0, Color.GREEN, screen, p));
		actors.add(new VerticalMovingPlatform(100, 115 - 600, 200, 15,
				Color.BLACK, screen));
		actors.add(new VerticalMovingPlatform(100, 115 - 800, 200, 15,
				Color.BLACK, screen));
		screen.addActors(actors);
		screen.paused = false;
		screen.play();
	}

	public static void loadPauseMenu(Screen screen) {
		screen.getContentPane().removeAll();
		GridBagConstraints c = new GridBagConstraints();
		c.insets = menuInsets;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 0;
		MenuLabel gamePausedLabel = new MenuLabel("GAME PAUSED");
		screen.getContentPane().add(gamePausedLabel, c);
		screen.validate();
		c.gridy++;
		MenuButton optionsButton = new MenuButton("OPTIONS");
		screen.getContentPane().add(optionsButton, c);
		screen.validate();
		c.gridy++;
		MenuButton restartButton = new MenuButton("RESTART LEVEL");
		screen.getContentPane().add(restartButton, c);
		screen.validate();
		c.gridy++;
		MenuButton cancelButton = new MenuButton("CANCEL");
		screen.getContentPane().add(cancelButton, c);
		screen.validate();
		c.gridy++;
		MenuButton quitButton = new MenuButton("QUIT");
		screen.getContentPane().add(quitButton, c);
		screen.validate();
		screen.requestFocus();
		while (quitButton.isEnabled() && restartButton.isEnabled()
				&& cancelButton.isEnabled() && screen.paused) {
			Screen.sleep(1, 0);
		}
		screen.paused = false;
		if (quitButton.isEnabled() == false) {
			loadMainMenu(screen);
		} else if (restartButton.isEnabled() == false) {
			loadRoom(currentLevel, screen);
		} else if (cancelButton.isEnabled() == false) {
			screen.getContentPane().removeAll();
		} else {
			screen.getContentPane().removeAll();
		}
	}

	public static void loadDeadMenu(Screen screen) {
		screen.getContentPane().removeAll();
		GridBagConstraints c = new GridBagConstraints();
		c.insets = menuInsets;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 0;
		MenuLabel gamePausedLabel = new MenuLabel("YOU DIED");
		screen.getContentPane().add(gamePausedLabel, c);
		screen.validate();
		c.gridy++;
		MenuButton restartButton = new MenuButton("RESTART LEVEL");
		screen.getContentPane().add(restartButton, c);
		screen.validate();
		c.gridy++;
		MenuButton quitButton = new MenuButton("QUIT");
		screen.getContentPane().add(quitButton, c);
		screen.validate();
		while (quitButton.isEnabled() && restartButton.isEnabled()) {
			Screen.sleep(1, 0);
		}
		if (quitButton.isEnabled() == false) {
			loadMainMenu(screen);
		} else if (restartButton.isEnabled() == false) {
			loadRoom(currentLevel, screen);
		}
	}

	public static ImageIcon getButtonIcon(Color backgroundColor,
			String buttonString) {
		BufferedImage buttonImage = new BufferedImage(1, 1,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) buttonImage.getGraphics();
		int buttonWidth = (int) g.getFontMetrics(buttonFont)
				.getStringBounds(buttonString, g).getWidth()
				+ 2 * BUTTON_TEXT_INSET;
		int stringDescent = (int) g.getFontMetrics(buttonFont).getDescent();
		buttonImage = new BufferedImage(buttonWidth, BUTTON_HEIGHT,
				BufferedImage.TYPE_INT_ARGB);
		g = (Graphics2D) buttonImage.getGraphics();
		g.setFont(buttonFont);
		// draws borders
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, buttonWidth, BUTTON_HEIGHT);
		g.setColor(Color.WHITE);
		g.fillRect(2, 2, buttonWidth - 4, BUTTON_HEIGHT - 4);
		// draws inside
		g.setColor(backgroundColor);
		g.fillRect(3, 3, buttonWidth - 6, BUTTON_HEIGHT - 6);
		g.setColor(Color.WHITE);
		// draws white border for text
		for (int r = -1; r <= 1; r++) {
			for (int c = -1; c <= 1; c++) {
				g.drawString(buttonString, BUTTON_TEXT_INSET + r, BUTTON_HEIGHT
						/ 2 + stringDescent + c);
			}
		}
		// draws text
		g.setColor(Color.BLACK);
		g.drawString(buttonString, BUTTON_TEXT_INSET, BUTTON_HEIGHT / 2
				+ stringDescent);
		return new ImageIcon(buttonImage);
	}

	public static ImageIcon getLabelIcon(String labelString) {
		BufferedImage buttonImage = new BufferedImage(1, 1,
				BufferedImage.TYPE_INT_ARGB);
		Graphics g = buttonImage.getGraphics();
		int buttonWidth = (int) g.getFontMetrics(labelFont)
				.getStringBounds(labelString, g).getWidth();
		int stringDescent = (int) g.getFontMetrics(labelFont).getDescent();
		buttonImage = new BufferedImage(buttonWidth, LABEL_HEIGHT,
				BufferedImage.TYPE_INT_ARGB);
		g = buttonImage.getGraphics();
		g.setFont(labelFont);
		g.setColor(Color.LIGHT_GRAY);
		// draws border for text
		for (int r = -2; r <= 2; r++) {
			for (int c = -2; c <= 2; c++) {
				g.drawString(labelString, r, LABEL_HEIGHT / 2 + stringDescent
						+ c);
			}
		}
		// draws text
		g.setColor(Color.BLACK);
		g.drawString(labelString, 0, LABEL_HEIGHT / 2 + stringDescent);
		return new ImageIcon(buttonImage);
	}

	public static class MenuButton extends JButton {

		public MenuButton(String myText) {
			// blue
			setIcon(getButtonIcon(Color.BLUE, myText));
			// dark blue
			setPressedIcon(getButtonIcon(Color.BLUE.darker(), myText));
			// light blue
			setRolloverIcon(getButtonIcon(new Color(60, 60, 255), myText));
			setBorder(null);
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					MenuButton.this.setEnabled(false);
				}
			});
		}
	}

	public static class MenuLabel extends JLabel {

		public MenuLabel(String myText) {
			setIcon(getLabelIcon(myText));
		}
	}

	public static void blackSpiral(Point2D center, Screen gamescreen) {
		try {
			Thread.sleep(750);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		gamescreen.paused = true;
		Point anchor = new Point((int) center.getX(), (int) center.getY());
		Rectangle2D blackWriter = new Rectangle2D.Double(anchor.x, anchor.y, 1,
				5);
		AffineTransform rotator = new AffineTransform();
		Graphics2D g = (Graphics2D) gamescreen.getContentPane().getGraphics();
		g.setColor(Color.BLACK);
		for (double i = 1; blackWriter.getWidth() < gamescreen.getContentPane()
				.getWidth(); i += .00003) {
			blackWriter.add(new Point2D.Double(blackWriter.getX()
					+ blackWriter.getWidth() + .4, blackWriter.getCenterY()));
			blackWriter
					.add(new Point2D.Double(blackWriter.getCenterX(),
							blackWriter.getY() + blackWriter.getHeight() + 50
									* (i - 1)));
			rotator.rotate(Math.toRadians(i), anchor.x, anchor.y);
			g.fill(rotator.createTransformedShape(blackWriter));
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		gamescreen.clearActors();
		gamescreen.getContentPane().setBackground(Color.BLACK);
	}
}
