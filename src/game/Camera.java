package game;

import java.awt.geom.Rectangle2D;

import actors.Actor;

public class Camera {
	public static final double sideBuffer = 350;

	public static final double START_X = sideBuffer,
			START_Y = Screen.GROUND_LEVEL - 300;

	public volatile static double x = START_X, y = START_Y, width = 300,
			height = 100;

	public static Screen gamescreen;
	public static Actor actorInFocus;

	public static void move() {
		moveX();
		moveY();
	}

	public static void moveX() {
		double actorX = actorInFocus.x;
		double actorWidth = actorInFocus.width;

		// if actor moves out of no-shift-area, move the area to enclose actor
		if (actorX < x) {
			x = actorX;
		} else if (actorX + actorWidth > x + width) {
			x += (actorX + actorWidth) - (x + width);
		}
		width = gamescreen.getContentPane().getWidth() - 2 * sideBuffer;
	}

	public static void moveY() {
		double actorY = actorInFocus.y;
		double actorHeight = actorInFocus.height;

		// if actor moves out of no-shift-area, move the area to enclose actor
		if (actorY < y) {
			y = actorY;
		} else if (actorY + actorHeight > y + height) {
			y += (actorY + actorHeight) - (y + height);
		}
		// stops camera from moving below the ground
		if (y > START_Y) {
			y = START_Y;
		}
	}

	public static void reset() {
		x = START_X;
		y = START_Y;
	}

	public static void setActorInFocus(Actor a) {
		actorInFocus = a;
		move();
	}

	/**
	 * @return Horizontal distance camera has moved since its initialization.
	 *         Used to paint actors in a visible area on the screen
	 */
	public static double getShiftX() {
		return x - START_X;
	}

	/**
	 * @return Vertical distance camera has moved since its initialization. Used
	 *         to paint actors in a visible area on the screen
	 */
	public static double getShiftY() {
		return y - START_Y - gamescreen.getHeight() + 600;
	}

	public static Rectangle2D.Double getNoShiftArea() {
		return new Rectangle2D.Double(x, y, width, height);
	}
}
