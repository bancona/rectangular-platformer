package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import actors.Actor;

@SuppressWarnings("serial")
public class Screen extends JFrame implements KeyListener, MouseListener {
	boolean isMinSize = true;
	public static final Color BACKGROUND_COLOR = Color.WHITE;
	public static final int MIN_WIDTH = 800, MIN_HEIGHT = 550,
			GROUND_LEVEL = 500, GROUND_HEIGHT = MIN_HEIGHT - GROUND_LEVEL,
			FRAMERATE = 100;

	public ArrayList<Actor> actors = new ArrayList<Actor>();
	public ArrayList<Actor> actorsToRemove = new ArrayList<Actor>();

	public volatile boolean paused;

	public static void main(String[] args) {
		Screen screen = new Screen("Platformer");
		Camera.gamescreen = screen;
		Rooms.loadMainMenu(screen);
	}

	public void play() {
		while (true) {
			if (!hasFocus()) {
				requestFocus();
			}
			for (Actor actor : actors) {
				actor.act();
			}
			removeDeadActors();
			sleep(1, 200000);
			if (paused) {
				Rooms.loadPauseMenu(this);
			}
		}
	}

	public void removeDeadActors() {
		synchronized (actors) {
			synchronized (actorsToRemove) {
				for (Actor deadActor : actorsToRemove) {
					actors.remove(deadActor);
				}
				actorsToRemove.clear();
			}
		}
	}

	public static void sleep(long ms, int ns) {
		try {
			Thread.sleep(ms, ns);
		} catch (InterruptedException e) {
		}
	}

	public Screen(String arg0) {
		super(arg0);
		JPanel contentPane = new JPanel(new GridBagLayout()) {
			@Override
			public void paint(Graphics g) {
				super.paint(g);
				paintActors(g);
				paintComponents(g);
			}
		};
		setContentPane(contentPane);
		addKeyListener(this);
		addMouseListener(this);
		validate();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(MIN_WIDTH, MIN_HEIGHT));
		if (isMinSize) {
			setToMinSize();
		} else {
			setToFullScreen();
		}
		getContentPane().setBackground(BACKGROUND_COLOR);
		setVisible(true);
		addRepaintTimer();
	}

	public void setToFullScreen() {
		setSize(GraphicsEnvironment.getLocalGraphicsEnvironment()
				.getMaximumWindowBounds().width, GraphicsEnvironment
				.getLocalGraphicsEnvironment().getMaximumWindowBounds().height);
		setLocation(0, 0);
	}

	public void setToMinSize() {
		setSize(MIN_WIDTH, MIN_HEIGHT);
		setLocationRelativeTo(null);// centers frame
	}

	public void addActor(Actor actor) {
		synchronized (actors) {
			actors.add(actor);
		}
	}

	public void addActors(Collection<Actor> actorsToAdd) {
		synchronized (actors) {
			for (Actor actor : actorsToAdd) {
				actors.add(actor);
			}
		}
	}

	public void paintActors(Graphics g) {
		synchronized (actors) {
			for (Actor a : actors) {
				a.paint(g);
			}
		}
	}

	public void addRepaintTimer() {
		new Timer(1000 / FRAMERATE, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!paused) {
					repaint();
				}
			}
		}).start();
	}

	public void clearActors() {
		synchronized (actors) {
			actors.clear();
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		for (Actor actor : actors) {
			actor.keyPressed(arg0);
		}
		if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
			paused = !paused;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		for (Actor actor : actors) {
			actor.keyReleased(arg0);
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		for (Actor a : actors) {
			a.mousePressed(arg0);
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		for (Actor a : actors) {
			a.mouseReleased(arg0);
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

}
