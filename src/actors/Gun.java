package actors;

import game.Camera;
import game.Screen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;

public class Gun {
	public final Actor owner;
	public double angle, width, height;
	public Rectangle displayBounds;
	public Screen gamescreen;
	public ArrayList<GunShot> activeShots, inactiveShots;

	public Gun(double w, double h, Color c, Screen s, Actor a) {
		gamescreen = s;
		width = w;
		height = h;
		owner = a;
		// default gun is centered at center of owner
		displayBounds = new Rectangle((int) (a.getCenterX() - width / 2),
				(int) (a.getCenterY() - height / 2), (int) width, (int) height);
		activeShots = new ArrayList<GunShot>();
		inactiveShots = new ArrayList<GunShot>();
	}

	public void moveShots() {
		synchronized (activeShots) {
			for (GunShot shot : activeShots) {
				shot.act();
			}
		}
		removeInactiveShots();
	}

	public void paint(Graphics g) {
		paintShots(g);
	}

	public void paintShots(Graphics g) {
		synchronized (activeShots) {
			for (GunShot shot : activeShots) {
				shot.paint(g);
			}
		}
	}

	public void fire() {
		if (owner.isDead == false) {
			synchronized (activeShots) {
				activeShots.add(new GunShot(displayBounds.getCenterX()
						+ Camera.getShiftX(), displayBounds.getCenterY()
						+ Camera.getShiftY(), angle, this));
			}
		}
	}

	public void removeInactiveShots() {
		synchronized (inactiveShots) {
			synchronized (activeShots) {
				for (GunShot shot : inactiveShots) {
					activeShots.remove(shot);
				}
				inactiveShots.clear();
			}
		}

	}

	public void shotHit(GunShot gs) {
		synchronized (inactiveShots) {
			inactiveShots.add(gs);
		}
	}
}
