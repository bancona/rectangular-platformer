package actors.hero;

import game.Screen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;

import actors.Actor;
import actors.Gun;

public class HeroGun extends Gun {

	public HeroGun(double w, double h, Color c, Screen s, Actor a) {
		super(w, h, c, s, a);
	}

	public volatile int flashCount;

	/**
	 * Gun image is painted to point toward the position of the mouse
	 * 
	 * @param g
	 *            Graphics object with which to paint the gun on the screen
	 */
	@Override
	public void paint(Graphics g) {
		Point mouseLoc = gamescreen.getContentPane().getMousePosition();
		double heroCenterX = owner.getDisplayRect().getCenterX();
		double heroCenterY = owner.getDisplayRect().getCenterY();

		if (mouseLoc != null && owner.isDead == false) {
			angle = Math.atan2(mouseLoc.y - heroCenterY, mouseLoc.x
					- heroCenterX);
		}
		displayBounds.setLocation((int) (heroCenterX - HeroGun.this.width / 2),
				(int) (heroCenterY));

		// Rotates gun according to location of mouse
		AffineTransform aft = new AffineTransform();
		aft.rotate(angle, displayBounds.getCenterX(),
				displayBounds.getCenterY());
		Shape gunShape = aft.createTransformedShape(displayBounds);

		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.WHITE);
		g2d.fill(gunShape);
		g2d.setColor(Color.BLACK);
		g2d.draw(gunShape);
		if (flashCount > 0) {
			paintFlash(g);
			flashCount--;
		}
		paintShots(g);
	}

	public void paintFlash(Graphics g) {
		g.setColor(Color.RED);
		double startangle = Math.cos(angle) < 0 ? angle - Math.PI / 5 : angle
				+ Math.PI / 5;
		g.fillArc(
				(int) (displayBounds.getCenterX() + width / 2 * Math.cos(angle)),
				(int) (displayBounds.getCenterY() + width / 2 * Math.sin(angle) - 2),
				5, 5, (int) Math.toDegrees(startangle),
				(int) Math.toDegrees(4 / 3 * Math.PI));
		g.fillArc(
				(int) (displayBounds.getCenterX() + width / 2 * Math.cos(angle) - 1),
				(int) (displayBounds.getCenterY() + width / 2 * Math.sin(angle) - 2) - 1,
				5, 5, (int) Math.toDegrees(startangle),
				(int) Math.toDegrees(4 / 3 * Math.PI));
	}

	public boolean mousePressed;

	public void mousePressed(MouseEvent e) {
		// fires gun if gun pressed but not held
		if (!mousePressed) {
			fire();
			flashCount = 10;
			mousePressed = true;
		}
	}

	public void mouseReleased(MouseEvent e) {
		mousePressed = false;
	}

}
