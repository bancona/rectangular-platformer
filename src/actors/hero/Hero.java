package actors.hero;

import game.Camera;
import game.Options;
import game.Rooms;
import game.Screen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import actors.Actor;
import actors.ColType;
import actors.Collision;
import actors.enemies.HeroKiller;
import actors.platforms.Platform;

public class Hero extends Actor {

	public boolean leftPressed, rightPressed, jumpPressed, canJump,
			unCrouchPending, deadJumped;
	public Platform myPlatform = null;
	public double addJump, jumpSpeed, maxHorSpeed, airAcceleration,
			groundAcceleration, airDeceleration, groundDeceleration;
	public double unCrouchedHeight;
	// Controls: left/right arrows to move; Z to jump; down arrow to crouch
	public final int RIGHT = KeyEvent.VK_RIGHT, LEFT = KeyEvent.VK_LEFT,
			DOWN = KeyEvent.VK_DOWN, JUMP = KeyEvent.VK_Z;
	public HeroGun myGun;

	public Hero(double x, double y, Color c, Screen s) {
		super(x, y, 20, 30, c, s);
		unCrouchedHeight = height;
		canJump = true;
		addJump = .003;
		jumpSpeed = -1;
		maxHorSpeed = .8;
		airAcceleration = .007;
		groundAcceleration = .02;
		airDeceleration = .003;
		groundDeceleration = .015;
		Camera.setActorInFocus(this);
		myGun = new HeroGun(20, 5, Color.BLACK, gamescreen, this);
	}

	public Hero(double x, double y, double w, double h, Color c, Screen s) {
		super(x, y, w, h, c, s);
		unCrouchedHeight = height;
		canJump = true;
		addJump = .003;
		jumpSpeed = -1;
		maxHorSpeed = .8;
		airAcceleration = .007;
		groundAcceleration = .02;
		airDeceleration = .003;
		groundDeceleration = .015;
		Camera.setActorInFocus(this);
		myGun = new HeroGun(20, 5, Color.BLACK, gamescreen, this);
	}

	@Override
	public void actAlive() {
		super.actAlive();
		if (unCrouchPending && canUncrouch()) {
			uncrouch();
			unCrouchPending = false;
		}
		Camera.move();
		myGun.moveShots();
	}

	@Override
	public void actDead() {
		moveDead();
	}

	@Override
	public void horizontalMove() {
		x += horSpeed;
		// if on a moving platform, stay on it
		if (onPlatform()) {
			x += myPlatform.horChange;
		}
		if (leftPressed || rightPressed) {
			horizontalAccelerate();
		}
		horizontalDecelerate();
	}

	@Override
	public void verticalMove() {
		if (onPlatform() && checkStillOnPlatform() == false) {
			myPlatform = null;
		}
		if (onPlatform() == false) {
			y += vertSpeed;
			if (vertSpeed < Options.terminalVelocity) {
				vertSpeed += Options.gravity;
			}
			if (jumpPressed && vertSpeed < 0) {
				vertSpeed -= addJump;
			}
		}
		// if on a moving platform, stay on it
		if (onPlatform() && vertSpeed == 0) {
			y = myPlatform.y - height;
			// y += myPlatform.getVerticalChange();
		}
	}

	public void moveDead() {
		myColor = Color.BLACK;
		if (getDisplayY() <= gamescreen.getContentPane().getHeight()) {
			if (!deadJumped) {
				deadJump();
			}
			myPlatform = null;
			verticalMove();
		} else {
			Rooms.loadDeadMenu(gamescreen);
		}
	}

	/**
	 * Hero jumps when it dies (then falls through the bottom of the screen
	 * later in the animation)
	 */
	public void deadJump() {
		try {
			Thread.sleep(750);
		} catch (InterruptedException e) {
		} finally {
			vertSpeed = jumpSpeed;
			deadJumped = true;
		}
	}

	/**
	 * 
	 * @return True if standing position is not blocked, false if it is blocked
	 */
	public boolean canUncrouch() {
		boolean canUncrouch = true;
		uncrouch();
		for (Collision col : getCollisions()) {
			// if not just the actor standing on an object, can't straighten
			if (col.type != ColType.BOTTOM) {
				canUncrouch = false;
				break;
			}
		}
		crouch();
		return canUncrouch;
	}

	/**
	 * makes Hero 1/2 as tall
	 */
	public void crouch() {
		if (height == unCrouchedHeight) {
			height = unCrouchedHeight / 2;
			y += height;
		}
	}

	public boolean isDirectionalKeyPressed() {
		return leftPressed || rightPressed;
	}

	public Rectangle2D getIntersection(Actor a) {
		if (getHitBox().intersects(a.getHitBox())) {
			return getHitBox().createIntersection(a.getHitBox());
		}
		return null;
	}

	@Override
	public ArrayList<Collision> getCollisions() {
		ArrayList<Collision> allCollisions = new ArrayList<Collision>();
		for (Actor a : gamescreen.actors) {
			Rectangle2D collisionArea;
			// if non-real collision or collision with itself, do not add
			if (a.equals(this) || a.isDead
					|| !isRealRectangle(collisionArea = getIntersection(a))) {
				continue;
			}
			allCollisions.add(new Collision(getColType(collisionArea),
					collisionArea, a));
		}
		return allCollisions;
	}

	public ColType getColType(Rectangle2D collisionArea) {
		if (collisionArea.getHeight() > collisionArea.getWidth()) {
			if (collisionArea.getX() > x) {
				return ColType.RIGHT;
			}
			return ColType.LEFT;
		}
		// if (collisionArea.height <= collisionArea.width)
		if ((int) collisionArea.getY() <= (int) y) {
			return ColType.TOP;
		}
		return ColType.BOTTOM;
	}

	@Override
	public Rectangle2D getHitBox() {
		return new Rectangle2D.Double(x + horSpeed, y, width, height);
	}

	public void handleCollisions(ArrayList<Collision> cols) {
		for (Collision col : cols) {
			if (col.actor instanceof HeroKiller) {
				isDead = true;
			} else if (col.actor instanceof Goal) {
				Rooms.blackSpiral(getDisplayPoint(), gamescreen);
				Rooms.loadRoom(Rooms.currentLevel.getNext(), gamescreen);
			}
			typecheck: switch (col.type) {
			case TOP:
				if (vertSpeed < 0) {
					vertSpeed = 0;
				}
				y += col.area.getHeight();
				break typecheck;
			case BOTTOM:
				if (col.actor instanceof Platform) {
					myPlatform = (Platform) col.actor;
				}
				if (vertSpeed >= 0) {
					vertSpeed = 0;
					canJump = true;
				}
				y -= col.area.getHeight();
				break typecheck;
			case RIGHT:
				x -= col.area.getWidth();
				horSpeed = 0;
				break typecheck;
			case LEFT:
				x += col.area.getWidth();
				horSpeed = 0;
				break typecheck;
			}
		}
	}

	public void horizontalAccelerate() {
		double acceleration = onPlatform() ? groundAcceleration
				: airAcceleration;
		if (leftPressed) {
			horSpeed -= acceleration;
		} else if (rightPressed) {
			horSpeed += acceleration;
		}
		// cap horizontal speed at set maximum
		if (Math.abs(horSpeed) > maxHorSpeed) {
			if (horSpeed > 0) {
				horSpeed = maxHorSpeed;
			} else if (horSpeed < 0) {
				horSpeed = -maxHorSpeed;
			}
		}
	}

	public void horizontalDecelerate() {
		double deceleration = onPlatform() ? groundDeceleration
				: airDeceleration;
		if (horSpeed > 0) {
			horSpeed -= deceleration;
		} else if (horSpeed < 0) {
			horSpeed += deceleration;
		}
		if (!isDirectionalKeyPressed() && Math.abs(horSpeed) < deceleration) {
			horSpeed = 0;
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if (keyCode == LEFT) {
			leftPressed = true;
			rightPressed = false;
		} else if (keyCode == RIGHT) {
			rightPressed = true;
			leftPressed = false;
		} else if (keyCode == DOWN) {
			crouch();
			unCrouchPending = false;
		} else if ((keyCode == JUMP || keyCode == KeyEvent.VK_UP) && canJump
				&& onPlatform()) {
			jumpPressed = true;
			vertSpeed = jumpSpeed;
			myPlatform = null;
			y--; // to make sure not stuck on platform
			canJump = false;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if (keyCode == LEFT) {
			leftPressed = false;
		} else if (keyCode == RIGHT) {
			rightPressed = false;
		} else if (keyCode == DOWN) {
			unCrouchPending = true;
		} else if (keyCode == JUMP) {
			jumpPressed = false;
		}
	}

	public boolean checkStillOnPlatform() {
		return x + width >= myPlatform.x
				&& x <= myPlatform.x + myPlatform.width;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		myGun.paint(g);
	}

	public void paintInRealLocation(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(Color.GREEN);
		g2D.fill(Camera.getNoShiftArea());
		g2D.setColor(Color.BLACK);
		g2D.fill(getHitBox());
	}

	public void uncrouch() {
		if (height == unCrouchedHeight / 2) {
			y -= height;
			height = unCrouchedHeight;
		}
	}

	public boolean isRealRectangle(Rectangle2D r) {
		return r != null && r.getWidth() >= 0 && r.getHeight() >= 0;
	}

	public boolean onPlatform() {
		return myPlatform != null;
	}

	public void mousePressed(MouseEvent e) {
		if (myGun != null) {
			myGun.mousePressed(e);
		}
	}

	public void mouseReleased(MouseEvent e) {
		if (myGun != null) {
			myGun.mouseReleased(e);
		}
	}
}
