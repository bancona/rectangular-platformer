package actors.hero;

import game.Screen;

import java.awt.Color;

import actors.Actor;
import actors.NonSolidObject;

public class Goal extends Actor implements NonSolidObject {

	public Goal(double x, double y, Color c, Screen s) {
		super(x, y, 20, 30, c, s);
	}

	public Goal(double x, double y, double w, double h, Color c, Screen s) {
		super(x, y, w, h, c, s);
	}

}
