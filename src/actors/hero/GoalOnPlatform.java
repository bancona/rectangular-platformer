package actors.hero;

import game.Screen;

import java.awt.Color;

import actors.platforms.Platform;

public class GoalOnPlatform extends Goal {
	public Platform myPlatform;

	public GoalOnPlatform(double x, double y, Color c, Screen s, Platform p) {
		super(x, y, c, s);
		myPlatform = p;
		this.y = myPlatform.y - height;
		this.x = myPlatform.x + myPlatform.width / 2;
	}

	@Override
	public void verticalMove() {
		y = myPlatform.y - height;
	}

	@Override
	public void horizontalMove() {
		x = myPlatform.x + myPlatform.width / 2;
	}

}
