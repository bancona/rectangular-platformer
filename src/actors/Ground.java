package actors;

import game.Screen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import actors.platforms.Platform;

public class Ground extends Platform {
	public static final Color GROUND_COLOR = Color.GRAY;
	public static final double X = Integer.MIN_VALUE / 2;
	public static final double Y = Screen.GROUND_LEVEL;
	public static final double WIDTH = Integer.MAX_VALUE;
	public static final double HEIGHT = Integer.MAX_VALUE / 2;
	BufferedImage groundImage;

	public Ground(Screen s) {
		
		super(X, Y, WIDTH, HEIGHT, GROUND_COLOR, s);
		width = WIDTH;
		height = HEIGHT;/*l
		groundImage = new BufferedImage((int) width, (int) height,
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) groundImage.getGraphics(); // g.setColor(myColor);
		try {
			g2.setPaint(new TexturePaint(ImageIO.read(new File("ground.jpg")),
					new Rectangle(0, 0, 16, 16)));
		} catch (IOException e) {
		}
		g2.fillRect(0, 0, groundImage.getWidth(), groundImage.getHeight()); // System.out.println("ground paint");*/
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
	//	g.drawImage(groundImage, (int) x, (int) y, GROUND_COLOR, null);
	}

	@Override
	public Rectangle2D.Double getHitBox() {
		return new Rectangle2D.Double(X, Y, WIDTH, HEIGHT);
	}

}
