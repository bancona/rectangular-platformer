package actors;

import java.awt.geom.Rectangle2D;

public class Collision {
	public ColType type;
	public Rectangle2D area;
	public Actor actor;

	public Collision(ColType type, Rectangle2D area, Actor actor) {
		this.type = type;
		this.area = area;
		this.actor = actor;
	}

}
