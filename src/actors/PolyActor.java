package actors;

import game.Screen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class PolyActor extends Actor {

	public Polygon myShape;
	public BufferedImage myImage;

	public PolyActor(double x, double y, double r, int sides, Color c, Screen s) {
		super(x, y, createShape(sides, r, new Point2D.Double()).getBounds2D()
				.getWidth(), createShape(sides, r, new Point2D.Double())
				.getBounds2D().getHeight(), c, s);
		myShape = createShape(sides, r, new Point2D.Double(x, y));
		myImage = createImage(myShape);
	}

	public PolyActor(Polygon p, Color c, Screen s) {
		super(p.getBounds2D().getX(), p.getBounds2D().getY(), p.getBounds2D()
				.getWidth(), p.getBounds2D().getHeight(), c, s);
		myShape = p;
		myImage = createImage(myShape);
	}

	/**
	 * Creates a regular polygon with the specified number of sides whose bounds
	 * has the specified point as a top-left corner
	 * 
	 * @param sides
	 *            Number of sides for the polygon (>=3 for real Polygon)
	 * @param radius
	 *            Distance from center of polygon to each vertex
	 * @param topLeft
	 *            Top-left corner of the bounds of the Polygon as a Point2D
	 * @return The complete Polygon as specified
	 */
	public static Polygon createShape(int sides, double radius, Point2D topLeft) {
		Polygon shape = new Polygon();
		double innerAngle = (double) 2 * Math.PI / sides;
		double curAngle = (double) (Math.PI + innerAngle) / 2;
		// creates shape centered at origin
		for (int side = 0; side < sides; side++) {
			shape.addPoint((int) (Math.cos(curAngle) * radius),
					(int) (Math.sin(curAngle) * radius));
			curAngle += innerAngle;
		}
		// moves shape so that top-left corner of bounds is at point specified
		shape.translate((int) (topLeft.getX() - shape.getBounds2D().getX()),
				(int) (topLeft.getY() - shape.getBounds2D().getY()));
		return shape;
	}

	/**
	 * Creates a BufferedImage to display in the paint(Graphics g) method that
	 * is as large as the rectangular bounds of the Polygon
	 * 
	 * @param imageShape
	 *            Polygon to be painted into the image
	 * @return The completed image of the Polygon in a BufferedImage object
	 */
	public BufferedImage createImage(Polygon imageShape) {
		Rectangle2D shapeBounds = imageShape.getBounds2D();
		// creates polygon whose bounds have (0,0) for top-left corner
		Polygon shapeToPaint = new Polygon(imageShape.xpoints,
				imageShape.ypoints, imageShape.npoints);
		shapeToPaint.translate((int) -shapeBounds.getX(),
				(int) -shapeBounds.getY());
		// draws polygon to image and returns it
		BufferedImage img = new BufferedImage((int) shapeBounds.getWidth(),
				(int) shapeBounds.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) img.getGraphics();
		g.setColor(myColor);
		g.fillPolygon(shapeToPaint);
		g.setColor(Color.BLACK);
		g.drawPolygon(shapeToPaint);
		return img;
	}

	public static ArrayList<Line2D> getSides(Polygon p) {
		ArrayList<Line2D> pSides = new ArrayList<Line2D>();
		for (int side = 0; side < p.npoints; side++) {
			pSides.add(new Line2D.Double(p.xpoints[side], p.ypoints[side],
					p.xpoints[(side + 1) % p.npoints], p.ypoints[(side + 1)
							% p.npoints]));
		}
		return pSides;
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(myImage, (int) getDisplayX(), (int) getDisplayY(), null);
	}

	@Override
	public Rectangle2D getHitBox() {
		return myShape.getBounds2D();
	}

}
