package actors;

import game.Camera;
import game.Screen;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public abstract class Actor {
	public final double START_X, START_Y;
	public double x, y, width, height, horSpeed, vertSpeed;
	public final Color START_COLOR;
	public Color myColor;
	public boolean isDead, justDied;
	public double deadCounter, deadCounterLimit = 700;
	public Screen gamescreen;

	public enum Side {
		TOP(0), RIGHT(1), BOTTOM(2), LEFT(3);
		int index;

		private Side(int index) {
			this.index = index;
		}
	}

	public Actor(double x, double y, double w, double h, Color c, Screen s) {
		START_X = x;
		START_Y = y;
		this.x = START_X;
		this.y = START_Y;
		this.width = w;
		this.height = h;
		START_COLOR = myColor = c;
		gamescreen = s;
	}

	public void act() {
		if (isDead == false)
			actAlive();
		else
			actDead();
	}

	public void actAlive() {
		move();
		handleCollisions(getCollisions());
	}

	public void actDead() {
		if (++deadCounter == deadCounterLimit)
			remove();
	}

	public void move() {
		horizontalMove();
		verticalMove();
	}

	public void horizontalMove() {
	}

	public void verticalMove() {
	}

	public ArrayList<Collision> getCollisions() {
		return null;
	}

	public void handleCollisions(ArrayList<Collision> collisions) {
	}

	public void paint(Graphics g) {
		if (isDead == false) {
			Graphics2D g2D = (Graphics2D) g;
			g2D.setColor(myColor);
			g2D.fill(getDisplayRect());
			g2D.setColor(Color.BLACK);
			g2D.draw(getDisplayRect());
		} else {
			paintDead(g);
		}
	}

	/**
	 * Displays the next frame in a dissolving animation
	 * 
	 * @param g
	 *            Graphics object to use in displaying dead actor
	 */
	public void paintDead(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		g.setColor(Color.BLACK);
		for (int r = 0; r < width; r += 1) {
			for (int c = 0; c < height; c += 1) {
				if (Math.random() >= deadCounter / deadCounterLimit) {
					g2D.fill(new Rectangle2D.Double(getDisplayX() + r,
							getDisplayY() + c, 1, 1));
				}
			}
		}
	}

	public void die() {
		justDied = true;
		isDead = true;
	}

	public void remove() {
		synchronized (gamescreen.actorsToRemove) {
			gamescreen.actorsToRemove.add(this);
		}
	}

	public void reset() {
		x = START_X;
		y = START_Y;
		myColor = START_COLOR;
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public double getDisplayX() {
		return x - Camera.getShiftX();
	}

	public double getDisplayY() {
		return y - Camera.getShiftY();
	}

	public double getCenterX() {
		return x + width / 2;
	}

	public double getCenterY() {
		return y + height / 2;
	}

	public Point2D getDisplayPoint() {
		return new Point2D.Double(getDisplayX(), getDisplayY());
	}

	public Point2D.Double getLocation() {
		return new Point2D.Double(x, y);
	}

	public Rectangle2D getHitBox() {
		return new Rectangle2D.Double(x, y, width, height);
	}

	public Rectangle2D getDisplayRect() {
		return new Rectangle2D.Double(getDisplayX(), getDisplayY(), width,
				height);
	}

	public ArrayList<Line2D> getSides() {
		ArrayList<Line2D> sides = new ArrayList<Line2D>();
		sides.add(new Line2D.Double(x, y, x + width, y));
		sides.add(new Line2D.Double(x + width, y, x + width, y + height));
		sides.add(new Line2D.Double(x + width, y + height, x, y + height));
		sides.add(new Line2D.Double(x, y + height, x, y));
		return sides;
	}

}
