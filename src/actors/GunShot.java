package actors;

import game.Camera;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import actors.Actor.Side;
import actors.enemies.HeroKiller;

public class GunShot {
	public volatile double x, y, angle;
	public final double WIDTH = 6, HEIGHT = 6;
	public final Color SHOT_FILL = Color.WHITE, SHOT_BORDER = Color.BLACK;
	public double spinCounter, spin;
	public Gun gunFired;

	public Rectangle2D.Double shotShape;

	public GunShot(double x, double y, double a, Gun g) {
		// starts shot at end of gun
		gunFired = g;
		angle = a;
		this.x = x + gunFired.width / 2 * Math.cos(angle);
		this.y = y + gunFired.width / 2 * Math.sin(angle);
		shotShape = new Rectangle2D.Double(x, y, WIDTH, HEIGHT);
	}

	public void paint(Graphics g) {
		Shape shot = getRotatedShot();
		Graphics2D g2D = (Graphics2D) g;
		g2D.setColor(SHOT_FILL);
		g2D.fill(shot);
		g2D.setColor(SHOT_BORDER);
		g2D.draw(shot);
	}

	public Shape getRotatedShot() {
		// square shot rotates 45 degrees every so often
		shotShape.setRect(getDisplayX(), getDisplayY(), WIDTH, HEIGHT);
		AffineTransform aft = new AffineTransform();
		if (++spinCounter % 2 == 0)
			aft.rotate((double) Math.PI / 4, shotShape.getCenterX(),
					shotShape.getCenterY());
		return aft.createTransformedShape(shotShape);
	}

	public void act() {
		move();
		handleCollisions();
	}

	public void move() {
		x += 2 * Math.cos(angle);
		y += 2 * Math.sin(angle);
	}

	public void handleCollisions() {
		for (Actor a : gunFired.owner.gamescreen.actors) {
			// if shot hits an actor, it disappears and kills actor if enemy
			if (a.equals(gunFired.owner) || a instanceof NonSolidObject
					|| !a.getHitBox().contains(getCenterPt())) {
				continue;
			}
			if (a instanceof ShotReflector
					&& !a.getHitBox().contains(getHitBox())) {
				bounce(a);
				move();
			} else {// shot disappears and kills "a" if enemy
				gunFired.shotHit(this);
				if (a instanceof HeroKiller) {
					a.die();
				}
			}
		}
	}

	public void bounce(Actor a) {
		Side closeSide = getClosestSide(a);
		if (closeSide == null) {
			return;
		}
		switch (closeSide) {
		case TOP:
		case BOTTOM:
			angle = -angle;
			break;
		case LEFT:
		case RIGHT:
			angle = Math.PI - angle;
			break;
		}
	}

	public Side getClosestSide(Actor a) {
		Side intersectedSide = null;
		double minDist = Double.MAX_VALUE;
		for (int i = 0; i < a.getSides().size(); i++) {
			if (getHitBox().intersectsLine(a.getSides().get(i))) {
				double ctrSideDist = a.getSides().get(i)
						.ptLineDist(getCenterPt());
				if (ctrSideDist < minDist && isValidSide(Side.values()[i])) {
					minDist = ctrSideDist;
					intersectedSide = Side.values()[i];
				}
			}
		}
		return intersectedSide;
	}

	public boolean isValidSide(Side s) {
		return !((s == Side.TOP && isMovingUp())
				|| (s == Side.RIGHT && isMovingRight())
				|| (s == Side.BOTTOM && isMovingDown()) || (s == Side.LEFT && isMovingLeft()));
	}

	public boolean isMovingRight() {
		return Math.cos(angle) > 0;
	}

	public boolean isMovingLeft() {
		return Math.cos(angle) < 0;
	}

	public boolean isMovingUp() {
		return Math.sin(angle) < 0;
	}

	public boolean isMovingDown() {
		return Math.sin(angle) > 0;
	}

	public Rectangle2D.Double getHitBox() {
		return new Rectangle2D.Double(x, y, WIDTH, HEIGHT);
	}

	public Point2D.Double getCenterPt() {
		return new Point2D.Double(x + WIDTH / 2, y + HEIGHT / 2);
	}

	public double getDisplayX() {
		return x - Camera.getShiftX();
	}

	public double getDisplayY() {
		return y - Camera.getShiftY();
	}
}
