package actors.enemies;

import game.Screen;

import java.awt.Color;

import actors.Actor;
import actors.platforms.HorizontalMovingPlatform;
import actors.platforms.Platform;
import actors.platforms.VerticalMovingPlatform;

public class PlatformPacer extends Actor implements HeroKiller {
	public Platform myPlatform;
	public double counter;

	public PlatformPacer(double x, double y, Color c, Screen s, Platform p) {
		super(x, y, 20, 30, c, s);
		myPlatform = p;
		horSpeed = .003;
		this.y = myPlatform.y - height;
		this.x = myPlatform.x + myPlatform.width / 2;
	}

	public PlatformPacer(double x, double y, double w, double h, Color c,
			Screen s, Platform p) {
		super(x, y, w, h, c, s);
		myPlatform = p;
		horSpeed = .003;
		this.y = myPlatform.y - height;
		this.x = myPlatform.x + myPlatform.width / 2;
	}

	@Override
	public void move() {
		counter += horSpeed;
		counter %= 2 * Math.PI;
		super.move();
	}

	@Override
	public void verticalMove() {
		y = myPlatform.y - height;
	}

	@Override
	public void horizontalMove() {
		x = myPlatform.x + ((myPlatform.width - width) / 2)
				* (Math.sin(counter) + 1);
	}

	@Override
	public void actDead() {
		super.actDead();
		if (myPlatform instanceof VerticalMovingPlatform) {
			verticalMove();
		} else if (myPlatform instanceof HorizontalMovingPlatform) {
			horizontalMove();
		}
	}

}
