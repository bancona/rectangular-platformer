package actors.enemies;

import game.Screen;

import java.awt.Color;

import actors.Actor;
import actors.platforms.Platform;

public class PlatformFloater extends Actor implements HeroKiller {
	public Platform myPlatform;
	public double counter, floatHeight;

	public PlatformFloater(double x, double y, Color c, Screen s, Platform p) {
		super(x, y, 20, 30, c, s);
		myPlatform = p;
		this.y = myPlatform.y - height;
		this.x = myPlatform.getCenterX();
		vertSpeed = .005;
		floatHeight = 100;
	}

	public PlatformFloater(double x, double y, double w, double h, Color c,
			Screen s, Platform p) {
		super(x, y, w, h, c, s);
		myPlatform = p;
		this.y = myPlatform.y - height;
		this.x = myPlatform.getCenterX();
		vertSpeed = .005;
		floatHeight = 100;
	}

	@Override
	public void actDead() {
		super.actDead();
		move();
	}

	@Override
	public void move() {
		counter += vertSpeed;
		counter %= 2 * Math.PI;
		super.move();
	}

	@Override
	public void horizontalMove() {
		x = myPlatform.getCenterX() - width / 2
				+ ((myPlatform.width - width) / 2) * (Math.sin(counter));
	}

	@Override
	public void verticalMove() {
		y = myPlatform.y - height - (floatHeight / 2)
				* (Math.cos(counter * 2) + 1) + 1;
	}

}
