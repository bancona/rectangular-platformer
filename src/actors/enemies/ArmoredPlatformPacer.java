package actors.enemies;

import game.Screen;

import java.awt.Color;
import java.awt.Graphics;

import actors.Armored;
import actors.platforms.Platform;

public class ArmoredPlatformPacer extends PlatformPacer implements HeroKiller,
		Armored {

	public ArmoredPlatformPacer(double x, double y, Color c, Screen s,
			Platform p) {
		super(x, y, c, s, p);
	}

	public ArmoredPlatformPacer(double x, double y, double w, double h,
			Color c, Screen s, Platform p) {
		super(x, y, w, h, c, s, p);
	}

	/**
	 * Paints armor around actor after drawing original actor
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.BLACK);
		g.drawRect((int) getDisplayX(), (int) getDisplayY(), (int) width,
				(int) height);
		g.drawRect((int) getDisplayX() + 2, (int) getDisplayY() + 2,
				(int) width - 4, (int) height - 4);
		g.setColor(Color.GRAY);
		g.drawRect((int) getDisplayX() + 1, (int) getDisplayY() + 1,
				(int) width - 2, (int) height - 2);

	}

}
