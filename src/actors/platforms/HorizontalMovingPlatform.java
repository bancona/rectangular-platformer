package actors.platforms;

import game.Screen;

import java.awt.Color;

public class HorizontalMovingPlatform extends Platform {
	private double counter;
	private final double AMPLITUDE = 100;

	public HorizontalMovingPlatform(double x, double y, double w, double h, Color c,
			Screen s) {
		super(x, y, w, h, c, s);
		horSpeed = .003;
	}

	@Override
	public void move() {
		counter += horSpeed;
		counter %= 2 * Math.PI;
		super.move();
	}

	@Override
	public void horizontalMove() {
		double prevX = x;
		x = START_X + width / 2 + AMPLITUDE * (Math.sin(counter) + 1);
		horChange = x - prevX;
	}

}
