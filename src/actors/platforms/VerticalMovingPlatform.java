package actors.platforms;

import game.Screen;

import java.awt.Color;

public class VerticalMovingPlatform extends Platform {
	private double counter;
	private final double AMPLITUDE = 100;

	public VerticalMovingPlatform(double x, double y, double w, double h,
			Color c, Screen s) {
		super(x, y + 50, w, h, c, s);
		vertSpeed = .003;
	}

	@Override
	public void move() {
		vertChange = AMPLITUDE * (Math.sin(counter) + 1) - AMPLITUDE
				* (Math.sin(counter += vertSpeed) + 1);
		counter %= 2 * Math.PI;
		super.move();
	}

	@Override
	public void verticalMove() {
		// y = START_Y + height / 2 + AMPLITUDE * (Math.sin(counter) + 1);
		y += vertChange;
	}

}
