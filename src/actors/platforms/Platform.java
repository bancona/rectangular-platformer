package actors.platforms;

import game.Screen;

import java.awt.Color;
import java.awt.geom.Rectangle2D;

import actors.Actor;

public class Platform extends Actor {
	public volatile double horChange = 0, vertChange = 0;

	public Platform(double x, double y, double w, double h, Color c, Screen s) {
		super(x, y, w, h, c, s);
	}

	public Platform(double x, double y, Color c, Screen s) {
		super(x, y, 200, 15, c, s);
	}

	@Override
	public Rectangle2D.Double getHitBox() {
		return new Rectangle2D.Double(x, y, width, height);
	}

}
